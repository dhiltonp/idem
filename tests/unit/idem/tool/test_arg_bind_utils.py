import pytest


@pytest.mark.asyncio
async def test_parse_index(mock_hub, hub):
    """
    test rend.jinja.parse_nested_key renders correctly with nested keys
    """
    prepare_mock_hub(mock_hub, hub)

    key, index_digits = mock_hub.tool.idem.arg_bind_utils.parse_index("0[1][2][*]")
    assert key == "0"
    assert index_digits == ["1", "2", "*"]

    key, index_digits = mock_hub.tool.idem.arg_bind_utils.parse_index("test[*][1][0]")
    assert key == "test"
    assert index_digits == ["*", "1", "0"]

    key, index_digits = mock_hub.tool.idem.arg_bind_utils.parse_index("[21][*][0]")
    assert not key
    assert index_digits == ["21", "*", "0"]

    key, index_digits = mock_hub.tool.idem.arg_bind_utils.parse_index("[21][*][0]")
    assert not key
    assert index_digits == ["21", "*", "0"]

    with pytest.raises(Exception):
        mock_hub.tool.idem.arg_bind_utils.parse_index("[invalid][*][0]")


@pytest.mark.asyncio
async def test_traverse_dict_and_list_data_as_list(mock_hub, hub):
    """
    test hub.tool.idem.arg_bind_utils.parse_dict_and_list parses data and finds the value of the attribute correctly
    """
    prepare_mock_hub(mock_hub, hub)

    subnet_1 = {"resource_id": "subnet1", "vpc_id": {"0": "vpc1"}, "az": "az1"}
    subnet_2 = {"resource_id": "subnet2", "vpc_id": {"0": "vpc2"}, "az": "az2"}

    new_state_data_list = [subnet_1, subnet_2]

    attribute_list = ["[0]", "resource_id"]

    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data_list, attribute_list
    )

    assert ret == "subnet1"

    attribute_list = ["[0]", "vpc_id", "0"]
    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data_list, attribute_list
    )

    assert ret == "vpc1"


@pytest.mark.asyncio
async def test_traverse_dict_and_list_data_as_dict(mock_hub, hub):
    """
    test hub.tool.idem.arg_bind_utils.parse_dict_and_list parses data and finds the value of the attribute correctly
    """
    prepare_mock_hub(mock_hub, hub)

    subnet_1 = {
        "resource_id": "subnet1",
        "vpc_id": {"0": "vpc1"},
        "azs": ["az1", "az2"],
    }
    subnet_2 = {
        "resource_id": "subnet2",
        "vpc_id": {"0": "vpc2"},
        "azs": ["az1", "az2"],
    }

    new_state_data = {"0": subnet_1, "1": subnet_2}

    attribute_list = ["0", "resource_id"]

    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data, attribute_list
    )

    assert ret == "subnet1"

    attribute_list = ["0", "azs", "[0]"]
    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data, attribute_list
    )

    assert ret == "az1"

    attribute_list = ["0", "azs[0]"]
    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data, attribute_list
    )

    assert ret == "az1"

    attribute_list = ["0", "azs[1]"]
    ret = mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list(
        "test_state", new_state_data, attribute_list
    )

    assert ret == "az2"


def prepare_mock_hub(mock_hub, hub):
    mock_hub.tool.idem.arg_bind_utils.parse_index = (
        hub.tool.idem.arg_bind_utils.parse_index
    )
    mock_hub.tool.idem.arg_bind_utils.parse_dict_and_list = (
        hub.tool.idem.arg_bind_utils.parse_dict_and_list
    )
    mock_hub.tool.idem.arg_bind_utils.get_chunk_with_index = (
        hub.tool.idem.arg_bind_utils.get_chunk_with_index
    )
