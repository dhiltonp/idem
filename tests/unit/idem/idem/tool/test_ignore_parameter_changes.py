import inspect


def test_ignore_parameter_changes_simple(hub, mock_hub):
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes = (
        hub.idem.tool.ignore_changes.ignore_parameter_changes
    )
    params = {"test_key": "test_value", "cold_key": "cold_value"}
    param_signatures = {
        "test_key": inspect.Parameter(
            name="test_key", kind=inspect.Parameter.POSITIONAL_OR_KEYWORD, default=None
        )
    }
    ignore_rule = "test_key"
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes(
        ignore_changes=[ignore_rule], params=params, param_signatures=param_signatures
    )
    assert params["test_key"] is None
    assert params["cold_key"] == "cold_value"


def test_ignore_parameter_changes_nested_dict(hub, mock_hub):
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes = (
        hub.idem.tool.ignore_changes.ignore_parameter_changes
    )
    params = {"test_key": {"test_key_2": "test_value"}, "cold_key": "cold_value"}
    param_signatures = {
        "test_key": inspect.Parameter(
            name="test_key", kind=inspect.Parameter.POSITIONAL_OR_KEYWORD, default=None
        )
    }
    ignore_rule = "test_key:test_key_2"
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes(
        ignore_changes=[ignore_rule], params=params, param_signatures=param_signatures
    )
    assert params["test_key"]["test_key_2"] is None
    assert params["cold_key"] == "cold_value"


def test_ignore_parameter_changes_nested_list(hub, mock_hub):
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes = (
        hub.idem.tool.ignore_changes.ignore_parameter_changes
    )
    params = {"test_key": [["a"], ["b"]]}
    param_signatures = {
        "test_key": inspect.Parameter(
            name="test_key", kind=inspect.Parameter.POSITIONAL_OR_KEYWORD, default=None
        )
    }
    ignore_rule = "test_key[0]"
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes(
        ignore_changes=[ignore_rule], params=params, param_signatures=param_signatures
    )
    assert params["test_key"][0] is None
    assert params["test_key"][1] == ["b"]


def test_ignore_parameter_changes_nested_list_dict(hub, mock_hub):
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes = (
        hub.idem.tool.ignore_changes.ignore_parameter_changes
    )
    params = {"test_key": [{"a": "b"}, {"c": "d"}]}
    param_signatures = {
        "test_key": inspect.Parameter(
            name="test_key", kind=inspect.Parameter.POSITIONAL_OR_KEYWORD, default=None
        )
    }
    ignore_rule = "test_key[0]:a"
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes(
        ignore_changes=[ignore_rule], params=params, param_signatures=param_signatures
    )
    assert params["test_key"][0] == {"a": None}
    assert params["test_key"][1] == {"c": "d"}


def test_ignore_parameter_changes_nested_list_all(hub, mock_hub):
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes = (
        hub.idem.tool.ignore_changes.ignore_parameter_changes
    )
    params = {"test_key": [{"a": "b"}, {"a": "c"}]}
    param_signatures = {
        "test_key": inspect.Parameter(
            name="test_key", kind=inspect.Parameter.POSITIONAL_OR_KEYWORD, default=None
        )
    }
    ignore_rule = "test_key[*]:a"
    mock_hub.idem.tool.ignore_changes.ignore_parameter_changes(
        ignore_changes=[ignore_rule], params=params, param_signatures=param_signatures
    )
    assert params["test_key"][0] == {"a": None}
    assert params["test_key"][1] == {"a": None}
