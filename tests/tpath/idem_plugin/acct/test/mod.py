async def gather(hub, profiles):
    result = {}
    for profile_name, profile in profiles.get("test.mod", {}).items():
        result[profile_name] = profile
        result[profile_name]["modded"] = True
    return result
