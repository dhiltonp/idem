"""Reconciliation will stop when result is True"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns failure then success
    """
    execution_count = (
        ctx.get("old_state", {}).get("execution_count", 0)
        if ctx.get("old_state")
        else 0
    )
    execution_count += 1

    result = False
    old_state = {}
    if execution_count >= 2:
        old_state = {
            "number_of_reruns": number_of_reruns,
            "execution_count": execution_count,
        }
        result = True

    return {
        "name": name,
        "result": result,
        "comment": "test",
        "old_state": old_state,
        "new_state": {
            "number_of_reruns": number_of_reruns,
            "execution_count": execution_count,
        },
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_clearing_rerun_data.present": []}}
