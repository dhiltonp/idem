"""Reconciliation will stop it as soon as rerun_data is reset to None"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns success in state execution
    """
    execution_count = (
        ctx.get("rerun_data", {}).get("execution_count", 0)
        if ctx.get("rerun_data")
        else 0
    )
    execution_count += 1

    rerun_data = {"execution_count": execution_count}
    old_state = {}
    if execution_count >= 2:
        rerun_data = None
        old_state = {
            "number_of_reruns": number_of_reruns,
            "execution_count": execution_count,
        }

    return {
        "name": name,
        "result": True,
        "rerun_data": rerun_data,
        "comment": "test",
        "old_state": old_state,
        "new_state": {
            "number_of_reruns": number_of_reruns,
            "execution_count": execution_count,
        },
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_clearing_rerun_data.present": []}}
