__contracts__ = ["resource"]


async def present(hub, ctx, name: str, resource_id):
    ...


async def absent(hub, ctx, name: str, resource_id):
    ...


async def describe(hub, ctx):
    return {}
