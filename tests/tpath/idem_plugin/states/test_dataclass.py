__contracts__ = ["resource"]

from dataclasses import make_dataclass, field
from typing import List


def present(
    hub,
    ctx,
    name: str,
    arg_dc_flat: make_dataclass(
        "ArgDcFlat",
        [
            ("ArgWithNone", str, field(default=None)),
            ("ArgWithBoolTrue", bool, field(default=True)),
            ("ArgWithBoolFalse", bool, field(default=False)),
            ("ArgInt", int, field(default=10)),
            (
                "arg_inner",
                make_dataclass(
                    "ArgInner",
                    [
                        ("InnerNone", str, field(default=None)),
                        ("InnerBoolTrue", bool, field(default=True)),
                        ("InnerBoolFalse", bool, field(default=False)),
                        ("InnerInt", int, field(default=20)),
                    ],
                ),
                field(default=None),
            ),
        ],
    ) = None,
    arg_dc_list: List[
        make_dataclass(
            "dc_type_in_list",
            [
                ("ArgWithNone", str, field(default=None)),
                ("ArgWithBoolTrue", bool, field(default=True)),
                ("ArgWithBoolFalse", bool, field(default=False)),
                ("ArgInt", int, field(default=10)),
            ],
        )
    ] = None,
    arg_dc_default_in_inner: make_dataclass(
        "DefaultInInner",
        [
            ("ArgWithNone", str, field(default=None)),
            (
                "arg_inner",
                make_dataclass(
                    "ArgInner",
                    [
                        ("InnerNone", str, field(default=None)),
                        ("InnerBoolTrue", bool, field(default=True)),
                        ("InnerBoolFalse", bool, field(default=False)),
                        ("InnerInt", int, field(default=20)),
                    ],
                ),
                field(default=None),
            ),
            (
                "arg_inner_different_default",
                make_dataclass(
                    "ArgInnerDifferentDefault",
                    [
                        ("InnerNone", str, field(default=None)),
                        ("InnerBoolTrue", bool, field(default=True)),
                        ("InnerBoolFalse", bool, field(default=False)),
                        ("InnerInt", int, field(default=40)),
                    ],
                ),
                field(default=None),
            ),
        ],
    ) = None,
):
    """
    Return the arguments of type dataclass in the new_state to verify how
    default values are populated for dataclass arguments.
    """
    ret = {
        "name": name,
        "old_state": None,
        "new_state": {
            "changed": "value",
            "arg_flat": arg_dc_flat,
            "arg_list": arg_dc_list,
            "arg_dc_default_in_inner": arg_dc_default_in_inner,
        },
        "result": True,
        "comment": None,
    }
    return ret


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"test_refresh.present": []}}
