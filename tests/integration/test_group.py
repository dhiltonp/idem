STATE_RESULTS = {
    "test_|-a_|-a_|-nop": {
        "tag": "test_|-a_|-a_|-nop",
        "name": "a",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-a_|-a_|-",
        "__run_num": 3,
        "start_time": "2022-08-11 14:00:43.860495",
        "total_seconds": 0.001162,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "test_|-b_|-b_|-nop": {
        "tag": "test_|-b_|-b_|-nop",
        "name": "b",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-b_|-b_|-",
        "__run_num": 4,
        "start_time": "2022-08-11 14:00:43.861726",
        "total_seconds": 0.001002,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "test_|-a1_|-a1_|-nop": {
        "tag": "test_|-a1_|-a1_|-nop",
        "name": "a1",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-a1_|-a1_|-",
        "__run_num": 5,
        "start_time": "2022-08-11 14:00:43.862789",
        "total_seconds": 0.00101,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "time_|-sleep_|-sleep_|-sleep": {
        "tag": "time_|-sleep_|-sleep_|-sleep",
        "name": "sleep",
        "changes": {},
        "new_state": {},
        "old_state": {},
        "comment": ["Successfully slept for 1 seconds."],
        "rerun_data": None,
        "result": True,
        "esm_tag": "time_|-sleep_|-sleep_|-",
        "__run_num": 1,
        "start_time": "2022-08-11 14:00:43.863861",
        "total_seconds": 1.003756,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "test_|-a3_|-a3_|-nop": {
        "tag": "test_|-a3_|-a3_|-nop",
        "name": "a3",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-a3_|-a3_|-",
        "__run_num": 6,
        "start_time": "2022-08-11 14:00:43.864774",
        "total_seconds": 0.001008,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "test_|-c_|-c_|-nop": {
        "tag": "test_|-c_|-c_|-nop",
        "name": "c",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-c_|-c_|-",
        "__run_num": 2,
        "start_time": "2022-08-11 14:00:43.865844",
        "total_seconds": 0.001001,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
    "test_|-a2_|-a2_|-nop": {
        "tag": "test_|-a2_|-a2_|-nop",
        "name": "a2",
        "changes": {},
        "new_state": None,
        "old_state": None,
        "comment": "Success!",
        "rerun_data": None,
        "result": True,
        "esm_tag": "test_|-a2_|-a2_|-",
        "__run_num": 7,
        "start_time": "2022-08-11 14:00:43.866906",
        "total_seconds": 0.001003,
        "sls_meta": {"SLS": {}, "ID_DECS": {}},
    },
}


def test_sort(hub):
    ret = hub.group.sort.apply(STATE_RESULTS)
    assert list(ret.keys()) == [
        "test_|-a1_|-a1_|-nop",
        "test_|-a2_|-a2_|-nop",
        "test_|-a3_|-a3_|-nop",
        "test_|-a_|-a_|-nop",
        "test_|-b_|-b_|-nop",
        "test_|-c_|-c_|-nop",
        "time_|-sleep_|-sleep_|-sleep",
    ]


def test_run_number(hub):
    ret = hub.group.number.apply(STATE_RESULTS)

    assert list(ret.keys()) == [
        "time_|-sleep_|-sleep_|-sleep",
        "test_|-c_|-c_|-nop",
        "test_|-a_|-a_|-nop",
        "test_|-b_|-b_|-nop",
        "test_|-a1_|-a1_|-nop",
        "test_|-a3_|-a3_|-nop",
        "test_|-a2_|-a2_|-nop",
    ]


def test_duration(hub):
    ret = hub.group.duration.apply(STATE_RESULTS)
    # the sleep state should be last because it took longest to run
    assert list(ret.keys())[-1] == "time_|-sleep_|-sleep_|-sleep"


def test_omit_noop(hub):
    ret = hub.group.omit_noop.apply(STATE_RESULTS)
    assert (
        ret["omit_noop_|-noop_count_|-_|-apply"]["comment"] == f"Total no-op states: 7"
    )


def test_pipe(hub):
    # Verify that pipes work and don't cause errors
    hub.OPT = {"idem": {"group": "init|init|init|init|init"}}
    ret = hub.group.init.apply(STATE_RESULTS)
    assert ret


def test_error(hub):
    # A non-existent group plugin shouldn't result in an error, just a log message
    hub.OPT = {"idem": {"group": "non-existent"}}
    ret = hub.group.init.apply(STATE_RESULTS)
    assert ret
