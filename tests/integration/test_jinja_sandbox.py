import jinja2
import pop.hub
import pytest
from dict_tools.data import NamespaceDict
from pytest_idem.runner import run_sls


def test_exec():
    """
    Test that you can do arg_binding inside jinja block
    """

    ret = run_sls(["arg_bind.arg_bind_in_jinja"])
    assert (
        ret["test_|-multi_result_new_state_|-multi_result_new_state_|-present"][
            "result"
        ]
        is True
    )

    new_state = ret.get(
        "test_|-multi_result_new_state_|-multi_result_new_state_|-present",
        {},
    ).get("new_state", {})

    expected_resource_ids = [v["resource_id"] for v in new_state.values()]

    assert (
        ret["trigger_|-test_arg_bind_in_jinja_|-test_arg_bind_in_jinja_|-build"][
            "result"
        ]
        is True
    )
    arg_binded_new_state = ret[
        "trigger_|-test_arg_bind_in_jinja_|-test_arg_bind_in_jinja_|-build"
    ]["new_state"]

    assert expected_resource_ids == arg_binded_new_state.get("triggers").get(
        "resource_ids"
    )
    assert "nested_value1" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_0"
    )
    assert "nested_value2" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_1"
    )
    assert "nested_value3" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_2"
    )


def test_jinja_hub_unsafe():
    """
    test rend.jinja.render throws unsafe exception when unsafe hub is accessed
    """

    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = NamespaceDict(
        rend=dict(enable_jinja_sandbox=True, jinja_sandbox_safe_hub_refs=["exec.*"])
    )

    with pytest.raises(jinja2.exceptions.SecurityError):
        run_sls(["arg_bind.arg_bind_in_jinja"], hub=hub)


def test_jinja_hub_safe():
    """
    test rend.jinja.render with safe hub access
    """

    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = NamespaceDict(
        rend=dict(
            enable_jinja_sandbox=True,
            jinja_sandbox_safe_hub_refs=["exec.*", "idem.arg_bind.*"],
        )
    )

    ret = run_sls(["arg_bind.arg_bind_in_jinja"])
    assert (
        ret["test_|-multi_result_new_state_|-multi_result_new_state_|-present"][
            "result"
        ]
        is True
    )


def test_disable_jinja_sandbox(idem_cli):
    ret = idem_cli("state", "--disable-jinja-sandbox", "--config-template")
    assert ret.json["rend"]["enable_jinja_sandbox"] is False


def test_disable_jinja_sandbox_defaults(idem_cli):
    ret = idem_cli("state", "--config-template")
    assert ret.json["rend"]["enable_jinja_sandbox"] is True


def test_jinja_sandbox_safe_hub_refs_defaults(idem_cli):
    ret = idem_cli("state", "--config-template")
    assert ret.json["rend"]["jinja_sandbox_safe_hub_refs"] == [
        "exec.*",
        "idem.arg_bind.*",
    ]
