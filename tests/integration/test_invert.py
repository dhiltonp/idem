import pathlib

from pytest_idem.runner import idem_cli
from pytest_idem.runner import run_sls

CACHE_DIR = ""


def test_normal(hub, tmpdir):
    global CACHE_DIR
    CACHE_DIR = pathlib.Path(tmpdir)
    ret = run_sls(
        ["success"], sls_offset="sls/invert", cache_dir=f"{CACHE_DIR / 'cache'}"
    )
    assert ret
    assert len(ret) == 6
    assert "test_|-first_|-first_|-nop" in ret
    assert "test_|-second_|-second_|-nop" in ret
    assert "test_|-third_|-third_|-nop" in ret
    assert "test_|-any_order_|-any_order_|-nop" in ret

    run_first = ret["test_|-first_|-first_|-nop"]["__run_num"]
    run_second = ret["test_|-second_|-second_|-nop"]["__run_num"]
    run_third = ret["test_|-third_|-third_|-nop"]["__run_num"]
    assert run_first < run_second
    assert run_second < run_third


def test_invert(hub, tmpdir):
    global CACHE_DIR
    CACHE_DIR = pathlib.Path(tmpdir)
    ret = run_sls(
        ["success"],
        sls_offset="sls/invert",
        invert_state=True,
        cache_dir=f"{CACHE_DIR / 'cache'}",
    )
    assert ret
    assert len(ret) == 6
    assert "test_|-first_|-first_|-nop" in ret
    assert "test_|-second_|-second_|-nop" in ret
    assert "test_|-third_|-third_|-nop" in ret
    assert "test_|-any_order_|-any_order_|-nop" in ret

    run_first = ret["test_|-first_|-first_|-nop"]["__run_num"]
    run_second = ret["test_|-second_|-second_|-nop"]["__run_num"]
    run_third = ret["test_|-third_|-third_|-nop"]["__run_num"]
    assert run_first < run_second
    assert run_second < run_third

    assert "test_|-test_invert_state_1_|-test_invert_state_1_|-absent" in ret
    assert "test_|-test_invert_state_2_|-test_invert_state_2_|-absent" in ret

    assert ret["test_|-test_invert_state_1_|-test_invert_state_1_|-absent"]["result"]
    assert ret["test_|-test_invert_state_2_|-test_invert_state_2_|-absent"]["result"]

    arg_bind_invert_1 = ret[
        "test_|-test_invert_state_1_|-test_invert_state_1_|-absent"
    ]["__run_num"]
    arg_bind_invert_2 = ret[
        "test_|-test_invert_state_2_|-test_invert_state_2_|-absent"
    ]["__run_num"]

    assert arg_bind_invert_1 > arg_bind_invert_2


def test_invert_in_nested_sls_run(tmpdir, tests_dir):
    global CACHE_DIR
    CACHE_DIR = pathlib.Path(tmpdir)
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'/ 'invert'}",
        f"--esm-plugin=local",
        f"--cache-dir={CACHE_DIR / 'cache'}",
        "init.sls",
    )
    ret = cmd_output["json"]
    assert len(ret) == 6
    for i in range(1, 4):
        assert (
            f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-present"
            in ret
        )
        assert (
            ret[
                f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-present"
            ]["new_state"]["name"]
            == f"param{i}"
        )
        assert (
            ret[
                f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-present"
            ]["new_state"]["resource_id"]
            == f"val{i}"
        )

    cmd_output1 = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run' / 'invert'}",
        f"--esm-plugin=local",
        f"--invert",
        f"--cache-dir={CACHE_DIR / 'cache'}",
        "init.sls",
    )
    ret = cmd_output1["json"]
    assert len(ret) == 6
    for i in range(1, 4):
        assert (
            f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-absent"
            in ret
        )
        assert (
            ret[
                f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-absent"
            ]["new_state"]["name"]
            == f"param{i}"
        )
        assert (
            ret[
                f"test_|-sls_run_state.sls_run_state_include.test_nested_state_param{i}_|-param{i}_|-absent"
            ]["new_state"]["resource_id"]
            == f"val{i}"
        )
