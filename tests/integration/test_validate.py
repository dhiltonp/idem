import json
import subprocess
import sys

from pytest_idem.runner import run_sls
from pytest_idem.runner import run_sls_validate


def assert_params(params):
    assert params
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""
    assert params["rg_name"]["your_rg"] == "default"
    assert params["locations"]
    assert isinstance(params["locations"], list)
    assert len(params["locations"]) == 5
    assert isinstance(params["locations"][4]["state"], dict)
    assert params["locations"][4]["state"]["city"] == ""


def test_validate_params(hub):
    ret = run_sls_validate(["success"])
    assert ret
    params = ret["params"].params()
    assert_params(params)

    parameters = ret["parameters"]
    assert_params(parameters["GLOBAL"])
    assert parameters["ID_DECS"]
    parameters_state = parameters["ID_DECS"]
    assert "sls_refs" in ret
    sls_ref = next(iter(ret["sls_refs"].keys()))
    assert (
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    assert (
        sls_ref + ".allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    params = parameters_state[
        sls_ref + ".allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"{sls_ref}.{{{{ params.get('{key}') }}}}"
        assert state_id in parameters_state
        params = parameters_state[state_id]
        assert key in params
        assert isinstance(params[key], str)
        assert params[key] == ""

    __validate_sls_param(ret, "success")

    warnings = ret["warnings"]
    assert warnings
    assert warnings["ID_DECS"]
    assert warnings["ID_DECS"][
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    id_warnings = warnings["ID_DECS"][
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert id_warnings["params_meta_missing"]
    assert "rg_name.your_rg" in id_warnings["params_meta_missing"]
    assert "locations" in id_warnings["params_meta_missing"]
    assert "locations[].state" in id_warnings["params_meta_missing"]

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"{sls_ref}.{{{{ params.get('{key}') }}}}"
        assert warnings["ID_DECS"][state_id]
        id_warnings = warnings["ID_DECS"][state_id]
        assert "params_meta_missing" in id_warnings
        assert key in id_warnings["params_meta_missing"]


def test_validate_policy_params(hub):
    ret = run_sls_validate(["policy"])
    assert ret

    __validate_sls_param(ret, "policy")

    assert ret["parameters"]
    assert ret["parameters"]["GLOBAL"]
    assert "tag_key_value" in ret["parameters"]["GLOBAL"]
    assert ret["parameters"]["ID_DECS"]
    for _, value in ret["parameters"]["ID_DECS"].items():
        assert "organization_unit_id" not in value.keys()
        assert "tag_key_value" in value.keys()


def test_validate_with_metadata(hub):
    ret = run_sls_validate(["meta"])
    assert ret

    __validate_sls_param(ret, "meta")

    assert ret["meta"]
    assert ret["meta"]["SLS"]
    assert ret["meta"]["ID_DECS"]
    for key, value in ret["meta"]["ID_DECS"].items():
        assert str(key).startswith("meta")
    assert ret["meta"]["ID_DECS"]["meta.happy"]["deep"] == "id_space"


def test_validate_with_metadata_parameters_and_arg_bind(hub):
    ret = run_sls_validate(["parameter-meta-arg-bind"])
    assert ret
    # Validate high and low
    __validate_sls_param(ret, "parameter-meta-arg-bind")
    assert len(ret["high"]) == len(ret["low"]) == 1


def test_validate_with_bool_parameter(hub):
    ret = run_sls_validate(["parameter-with-bool-value"])
    assert ret
    # Validate high and low
    __validate_sls_param(ret, "parameter-with-bool-value")
    for field in ret["high"].get("The Org policy").get("test.state"):
        if isinstance(field, dict) and field.get("is_enabled"):
            assert "('is_enabled', false)" in field.get("is_enabled"), field.get(
                "is_enabled"
            )
            return


def test_invalid_syntax(hub):
    # Verify a descriptive message is thrown with invalid syntax
    try:
        run_sls(["invalid_syntax"])
    except Exception as e:
        assert "Invalid syntax" in e.args[0]


def test_duplicate_declaration_id(runpy, tests_dir, mock_time):
    cmd = [
        sys.executable,
        runpy,
        "validate",
        tests_dir / "sls" / "validate" / "nested" / "init.sls",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret
    assert ret.returncode == 1
    assert "Duplicate state declarations found in SLS tree" in ret.stdout


def test_validate_cli(runpy, tests_dir, mock_time):
    cmd = [
        sys.executable,
        runpy,
        "validate",
        tests_dir / "sls" / "target.sls",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    output = json.loads(ret.stdout)

    __validate_sls_param(output, "target")

    assert 5 == len(output), len(output)


def test_validate_cli_with_meta(runpy, tests_dir, mock_time):
    cmd = [
        sys.executable,
        runpy,
        "validate",
        tests_dir / "sls" / "validate" / "meta.sls",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    output = json.loads(ret.stdout)

    __validate_sls_param(output, "meta")

    assert 5 == len(output), len(output)
    assert output["meta"]
    assert output["meta"]["SLS"]
    for key, value in output["meta"]["SLS"].items():
        assert key is not None
        assert ".meta" in str(key)
        assert value is not None

    assert output["meta"]["ID_DECS"]
    for key, value in output["meta"]["ID_DECS"].items():
        assert key is not None
        assert ".meta." in str(key)
        assert value is not None


def test_validate_param_json_input(hub):
    # test ensures parameters with json input values validate passes.
    ret = run_sls_validate(["parameter-with-json-value"])
    assert ret

    __validate_sls_param(ret, "parameter-with-json-value")

    assert ret["parameters"]
    assert ret["parameters"]["GLOBAL"]
    assert "tag_key_value" in ret["parameters"]["GLOBAL"]
    assert ret["parameters"]["ID_DECS"]
    for _, value in ret["parameters"]["ID_DECS"].items():
        assert "tag_key_value" in value.keys()


def test_validate_nested_param_json_input(hub):
    # test ensures nested parameters with json input values validate passes.
    ret = run_sls_validate(["parameter-with-nested-json-value"])
    assert ret

    __validate_sls_param(ret, "parameter-with-nested-json-value")

    assert ret["parameters"]
    assert ret["parameters"]["GLOBAL"]
    assert "tag_key_value" in ret["parameters"]["GLOBAL"]
    assert ret["parameters"]["ID_DECS"]
    for _, value in ret["parameters"]["ID_DECS"].items():
        assert "tag_key_value" in value.keys()


def test_validate_list_input(hub):
    # test ensures list parameters in Global.
    ret = run_sls_validate(["parameter-with-list-value"])
    assert ret

    __validate_sls_param(ret, "parameter-with-list-value")

    assert ret["parameters"]
    assert ret["parameters"]["GLOBAL"]
    assert "org_id" in ret["parameters"]["GLOBAL"]
    assert type(ret["parameters"]["GLOBAL"]["org_id"]) == list
    for _, value in ret["parameters"]["ID_DECS"].items():
        assert "org_id" in value.keys()


def __validate_sls_param(output, expected_sls_param):
    for key in output["high"]:
        sls_param = output["high"][key]["__sls__"]
        # It should match with the sls being validated
        assert sls_param == expected_sls_param or sls_param.endswith(expected_sls_param)

    # low is a list
    for item in output["low"]:
        sls_param = item["__sls__"]
        # It should match with the sls being validated
        assert sls_param == expected_sls_param or sls_param.endswith(expected_sls_param)
