def test_version(idem_cli, hub):
    ret = idem_cli("exec", "esm.version")
    assert ret.json.result is True
    assert ret.json.ret == ".".join(str(x) for x in hub.esm.VERSION)


def test_esm_unlock_provider_required(idem_cli, hub):
    ret = idem_cli("exec", "esm.unlock")
    assert ret.result is False
    assert "missing 1 required positional argument: 'provider'" in ret.stderr


def test_esm_show(idem_cli, hub):
    ret = idem_cli("exec", "esm.show")
    assert ret.result is True
    assert ret.json == {}


def test_esm_remove(idem_cli, hub):
    ret = idem_cli("exec", "esm.remove", "tag=any-tag")
    assert ret.result is True
    assert "Cannot find resource with tag 'any-tag' in ESM" in ret.stdout


def test_esm_remove_tag_required(idem_cli, hub):
    ret = idem_cli("exec", "esm.remove")
    assert ret.result is False
    assert "missing 1 required positional argument: 'tag'" in ret.stderr
