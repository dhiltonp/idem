import pytest


def test_fail(hub):
    with pytest.raises(ValueError):
        hub.exec.tests.returns.fail()


def test_malformed(hub):
    with pytest.raises(TypeError):
        hub.exec.tests.returns.malformed()


def test_malformed_dict(hub):
    with pytest.raises(TypeError):
        hub.exec.tests.returns.malformed_dict()


def test_wrong_status_type(hub):
    with pytest.raises(ValueError):
        hub.exec.tests.returns.wrong_status_type()


def test_ping(hub):
    ret = hub.exec.tests.returns.ping()
    assert ret.comment == ""
    assert ret.ref == "exec.tests.returns.ping"
    assert ret.result is True
    assert bool(ret) == ret.result
    assert ret.ret is ...
