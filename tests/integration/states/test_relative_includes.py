import pytest_idem.runner as runner


def test_relative_include_tree(tests_dir):
    ret = runner.idem_cli("state", f"--tree={tests_dir / 'sls'}", "nested")
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_relative_include_tree_specify_init(tests_dir):
    ret = runner.idem_cli("state", f"--tree={tests_dir / 'sls' / 'nested'}", "init")
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_relative_include_sls_sources(tests_dir):
    ret = runner.idem_cli(
        "state", f"--sls-sources=file://{tests_dir / 'sls'}", "nested"
    )
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_relative_include_no_init(tests_dir):
    ret = runner.idem_cli("state", f"{tests_dir / 'sls' / 'nested'}")
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_relative_include_init(tests_dir):
    ret = runner.idem_cli("state", f"{tests_dir / 'sls' / 'nested' / 'init.sls'}")
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_relative_include_init_no_ext(tests_dir):
    ret = runner.idem_cli("state", f"{tests_dir / 'sls' / 'nested' / 'init'}")
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state_|-state_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_multiple_init_sls(tests_dir):
    ret = runner.idem_cli(
        "state",
        f"{tests_dir / 'sls' / 'nested' / 'dup' / 'init.sls'}",
        f"{tests_dir / 'sls' / 'nested' / 'dup2' / 'init.sls'}",
    )
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state2_|-state2_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )
    assert ret.json["test_|-test2_|-test2_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )


def test_multiple_init_and_duplicate_name_sls(tests_dir):
    tree = tests_dir / "sls" / "nested_cfg_dup"
    config_file = (tree / "idem.cfg").absolute()
    ret = runner.idem_cli(
        "state",
        f"--config={config_file}",
    )
    assert ret.result, ret.stderr or ret.stdout
    assert ret.json["test_|-state1_|-state1_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )
    assert ret.json["test_|-state2_|-state2_|-nop"]["result"] is True, (
        ret.stderr or ret.stdout
    )
