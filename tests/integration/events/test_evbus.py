import asyncio
import json

import pytest

from .conftest import KAFKA_PROFILE_NAME
from .conftest import RABBITMQ_PROFILE_NAME


async def test_exec(hub, event_loop):
    body = "message body"
    profile = "test_idem_ingress"
    ingress_profiles = {"internal": [{profile: {}}]}

    listener = event_loop.create_task(hub.evbus.init.start(ingress_profiles))
    await hub.evbus.init.join()

    # Put an event on the queue
    try:
        ret = await hub.exec.test.event(body=body, ingress_profile=profile)
        assert ret.result is True
        queue: asyncio.Queue = await hub.ingress.internal.get()
        msg = await queue.get()

        assert json.loads(msg) == {
            "tags": {},
            "message": "message body",
            "run_name": "cli",
        }
    finally:
        await hub.evbus.init.stop()
        await listener


async def test_kafka(hub, kafka, idem_cli):
    body = "message body"

    # Fire an event using the cli
    idem_cli(
        "exec",
        "test.event",
        f'body="{body}"',
        f"ingress_profile={KAFKA_PROFILE_NAME}",
        f"--acct-file={hub.OPT.acct.acct_file}",
        f"--acct-key={hub.OPT.acct.acct_key or ''}",
    )

    # Verify that it was received in kafka
    received_message = await kafka.getone()

    assert json.loads(received_message.value) == {
        "message": "message body",
        "tags": {},
        "run_name": "cli",
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "event_kafka_custom_acct_file", ["idem-exec", "idem-*"], indirect=True
)
async def test_kafka_large_message_failure(
    hub, kafka, event_kafka_custom_acct_file, idem_cli
):
    # creating a relatively large message
    body = "".join(["large message body generator text"] * 100)

    idem_cli(
        "exec",
        "test.event",
        f'body="{body}"',
        f"ingress_profile={KAFKA_PROFILE_NAME}",
        env={"ACCT_KEY": "", "ACCT_FILE": event_kafka_custom_acct_file},
        # This should be the default and one can override it
        # f"--error-callback-ref=idem.event.publish_error_callback_handler"
    )

    # Verify that it was received in kafka
    received_message = await kafka.getone()
    idem_message = json.loads(received_message.value)

    assert idem_message
    assert idem_message.get("publish_event_error_message")
    assert "MessageSizeTooLargeError" in idem_message.get("publish_event_error_message")


async def test_rabbitmq(hub, rabbitmq, idem_cli):
    body = "message body"

    # Fire an event using the cli
    idem_cli(
        "exec",
        "test.event",
        f'body="{body}"',
        f"ingress_profile={RABBITMQ_PROFILE_NAME}",
        f"--acct-file={hub.OPT.acct.acct_file}",
        f"--acct-key={hub.OPT.acct.acct_key or ''}",
    )

    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == {
        "message": "message body",
        "tags": {},
        "run_name": "cli",
    }
