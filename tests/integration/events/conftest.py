import aio_pika
import aiokafka.structs
import kafka.errors
import pytest
import pytest_idem.runner as runner
import yaml

KAFKA_PROFILE_NAME = "test_development_evbus_kafka"
RABBITMQ_PROFILE_NAME = "test_development_evbus_pika"


@pytest.fixture
def acct_key(hub):
    yield hub.OPT.acct.acct_key


@pytest.fixture
async def kafka_ctx(hub, acct_key):
    if not hub.OPT.acct.acct_file:
        raise pytest.skip("No ACCT_FILE environment variable found for this test")

    profiles = await hub.acct.init.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=acct_key,
    )
    ctx = profiles.get("kafka", {}).get(KAFKA_PROFILE_NAME)
    if not ctx:
        raise pytest.skip(
            f"No profile named '{KAFKA_PROFILE_NAME}' in '{hub.OPT.acct.acct_file}'"
        )
    yield ctx


@pytest.fixture(name="kafka")
async def kafka_consumer(hub, kafka_ctx):
    """
    Start a local kafka server to run this test:
    .. code-block:: bash

        $ docker run --rm -p 9092:9092  \
            --env KAFKA_ENABLE_KRAFT=yes \
            --env KAFKA_CFG_PROCESS_ROLES=broker,controller \
            --env KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER \
            --env KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093 \
            --env KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT \
            --env KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://127.0.0.1:9092 \
            --env KAFKA_BROKER_ID=1 \
            --env KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@127.0.0.1:9093 \
            --env ALLOW_PLAINTEXT_LISTENER=yes \
            bitnami/kafka:latest

    Credentials for connecting:
    .. code-block:: sls

        kafka:
          test_development_evbus_kafka:
            connection:
              bootstrap_servers: localhost:9092
    """
    try:
        async with aiokafka.AIOKafkaConsumer(
            *kafka_ctx["topics"], **kafka_ctx["connection"]
        ) as consumer:
            # Start with a clean queue
            await consumer.getmany(timeout_ms=0)
            yield consumer
    except kafka.errors.KafkaConnectionError as e:
        raise pytest.skip(f"Could not connect to kafka server: {e}")


@pytest.fixture
async def pika_ctx(hub):
    if not hub.OPT.acct.acct_file:
        raise pytest.skip("No ACCT_FILE environment variables found for this test")

    profiles = await hub.acct.init.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=hub.OPT.acct.acct_key,
    )
    for provider in ("pika", "amqp", "rabbitmq"):
        ctx = profiles.get(provider, {}).get(RABBITMQ_PROFILE_NAME)
        if ctx:
            break
    else:
        raise pytest.skip(
            f"No profile named '{RABBITMQ_PROFILE_NAME}' in '{hub.OPT.acct.acct_file}'"
        )
    yield ctx


@pytest.fixture
async def rabbitmq(hub, event_loop, pika_ctx):
    """
    Start a local rabbitmq server to run this test:
    .. code-block:: bash

        $ docker run -p 5672:5672 \
          --env RABBITMQ_HOSTS=localhost \
          --env RABBITMQ_PORT=5672 \
          --env RABBITMQ_USER=guest \
          --env RABBITMQ_PASS=guest \
          --env RABBITMQ_PROTOCOL=amqp \
          rabbitmq:management

    Credentials for connecting:
    .. code-block:: sls

        pika:
          test_development_evbus_pika:
            connection:
              host: localhost
              port: 5672
              login: guest
              password: guest
    """
    try:
        conn: aio_pika.RobustConnection = await aio_pika.connect(
            loop=event_loop, **pika_ctx["connection"]
        )
    except ConnectionError as e:
        raise pytest.skip(f"Could not connect to rabbitmq server\n{e}")

    async with conn.channel() as channel:
        queue: aio_pika.RobustQueue = await channel.declare_queue(
            name=pika_ctx.get("routing_key", "")
        )
        # Start with a clean queue
        await queue.purge()
        yield queue

        await queue.delete(if_unused=False, if_empty=False)
        await conn.close()


@pytest.fixture
def event_acct_file(hub, pika_ctx, kafka_ctx, request, idem_cli):
    """
    Create a ctx that can be used to get all idem events based on the test profiles
    """
    # Get the event_profile_name from the parametrization I.E.
    #   @pytest.mark.parametrize("event_acct_file", ["<profile_name>"], indirect=True)
    event_profile_name = request.param

    with runner.named_tempfile(suffix=".sls") as fh:
        profiles = {
            "pika": [{event_profile_name: pika_ctx}],
            "kafka": {event_profile_name: kafka_ctx},
        }
        fh.write_text(yaml.safe_dump(profiles))

        yield str(fh)


@pytest.fixture
def event_kafka_custom_acct_file(hub, kafka_ctx, request, idem_cli):
    event_profile_name = request.param
    with runner.named_tempfile(suffix=".sls") as fh:
        kafka_ctx["connection"]["max_request_size"] = 500
        profiles = {
            "kafka": {event_profile_name: kafka_ctx},
        }
        fh.write_text(yaml.safe_dump(profiles))

        yield str(fh)
