import os
import sys

import pytest
import pytest_idem.runner as runner


@pytest.fixture(scope="module")
def cred_file():
    with runner.named_tempfile("w+", suffix=".yaml") as creds:
        creds.write_text("foo:\n- bar")
        yield str(creds)


@pytest.fixture(scope="module")
def acct_file():
    with runner.named_tempfile(suffix=".fernet") as f:
        yield str(f)


def test_encrypt(acct_key, cred_file, acct_file, idem_cli):
    idem_cli(
        "encrypt",
        cred_file,
        f'--acct-key="{acct_key}"',
        f"--output-file={acct_file}",
        "--crypto-plugin=fernet",
        check=True,
    )

    with open(acct_file) as fh:
        contents = fh.read()
        assert contents


def test_decrypt(acct_key, acct_file, idem_cli):
    ret = idem_cli(
        "decrypt",
        acct_file,
        f"--acct-key='{acct_key}'",
        "--crypto-plugin=fernet",
        check=True,
    )

    assert ret.json == {"foo": ["bar"]}


@pytest.mark.skipif(sys.platform != "linux", reason="This test only runs on linux")
def test_acct_edit_nokey(acct_file, idem_cli):
    ret = idem_cli(
        "acct_edit",
        acct_file,
        # Write some contents to the file with an automate-able editor
        r"--editor=sed -i '1s/.*/foo: bar/'",
        "--crypto-plugin=fernet",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
        check=True,
    )

    # Get the acct key that was printed as a result of the command
    acct_key = ret.stdout.strip()

    # Verify that the file can be decrypted and has the expected contents
    ret = idem_cli(
        "decrypt",
        acct_file,
        f"--acct-key='{acct_key}'",
        "--crypto-plugin=fernet",
        check=True,
    )

    assert ret.json == {"foo": "bar"}


@pytest.mark.skipif(sys.platform != "linux", reason="This test only runs on linux")
def test_acct_edit(acct_file, acct_key, idem_cli):
    # Let acct_edit create the file
    os.unlink(acct_file)

    idem_cli(
        "acct_edit",
        acct_file,
        # Write some contents to the file with an automate-able editor
        r"--editor=sed -i '1s/.*/foo: bar/'",
        f"--acct-key='{acct_key}'",
        check=True,
    )

    # Verify that the file can be decrypted and has the expected contents
    ret = idem_cli(
        "decrypt",
        acct_file,
        f"--acct-key='{acct_key}'",
        check=True,
    )

    assert ret.json == {"foo": "bar"}


def test_plaintext(idem_cli):
    """
    Test that exec modules can run with plaintext credentials
    """
    with runner.named_tempfile("w", suffix=".yaml") as creds:
        creds.write_text("test:\n  default:\n    key: value")

        ret = idem_cli(
            "exec",
            "tst_ctx.get",
            f"--acct-file={creds}",
            "--output=json",
            check=True,
            env={"ACCT_KEY": ""},
        )

    ret = ret.json["ret"]
    assert "acct" in ret
    assert ret["acct"] == {"key": "value"}


def test_extras(idem_cli):
    """
    Test that a json string in extras gets rendered properly
    """
    ret = idem_cli(
        "state",
        '--extras={"aws":{"esm":{"region_name":"test"}}}',
        "--output=json",
        "--config-template",
        check=True,
    )

    assert "acct" in ret.json
    assert "extras" in ret.json["acct"]
    assert ret.json["acct"]["extras"] == {"aws": {"esm": {"region_name": "test"}}}


def test_extras_not_supplied(idem_cli):
    """
    Test that no value passed to extras becomes a dictionary
    """
    ret = idem_cli(
        "state",
        "--output=json",
        "--config-template",
        check=True,
    )

    assert "acct" in ret.json
    assert "extras" in ret.json["acct"]
    assert ret.json["acct"]["extras"] == {}


def test_os_vars(idem_cli):
    """
    Test that os vars are read properly
    """
    ret = idem_cli(
        "decrypt",
        "file",
        "--output=json",
        "--config-template",
        env={
            "ACCT_FILE": "acct-file",
            "ACCT_KEY": "acct-key",
            "ACCT_PROFILE": "acct-profile",
        },
    )

    assert "acct" in ret.json
    assert "acct_file" in ret.json["acct"]
    assert ret.json["acct"]["acct_file"] == "acct-file"
    assert "acct_key" in ret.json["acct"]
    assert ret.json["acct"]["acct_key"] == "acct-key"
    assert "idem" in ret.json
    assert "acct_profile" in ret.json["idem"]
    assert ret.json["idem"]["acct_profile"] == "acct-profile"


def test_validation_no_profile(hub):
    """
    Test that os vars are read properly
    """
    STATE = """
    resource: resource_cloud.thing.present
    """
    hub.states.resource_cloud.ACCT = ["resource_cloud"]

    ret = runner.run_yaml_block(STATE, hub=hub)
    assert ret == ""


def test_validation_no_profile(hub):
    """
    Test that a cloud has the option to get profiles from a default profile
    """
    STATE = """
    resource: resource_cloud.thing.present
    """
    hub.states.resource_cloud.ACCT = ["resource_cloud"]

    ret = runner.run_yaml_block(STATE, hub=hub)

    # Verify that the gather function was called even though we didn't send any credentials
    assert hub.acct.resource_cloud.init.GATHER_CALLED is True

    assert ret["resource_cloud.thing_|-resource_|-resource_|-present"]["new_state"] == {
        "new_profile": "value"
    }


def test_validation_no_profile_no_acct(hub):
    """
    Test that a state can run when no ACCT is set up for the cloud
    """
    STATE = """
    resource: resource_cloud.thing.present
    """
    # hub.states.resource_cloud.ACCT = ["resource_cloud"]

    ret = runner.run_yaml_block(STATE, hub=hub)
    assert ret["resource_cloud.thing_|-resource_|-resource_|-present"]["result"] is True
