import pytest
import rend.exc
from pytest_idem.runner import IdemRunException
from pytest_idem.runner import run_sls


def test_include(hub):
    ret = run_sls(["include"])
    assert ret
    assert len(ret) == 5, f"Expecting 5 states: {ret}"
    for state_ref, state in ret.items():
        assert state["result"] is True, state_ref


def test_include_missing_files(hub):
    with pytest.raises(IdemRunException) as e:
        run_sls(["invalid_include"])
        assert "SLS ref 'dir.non_existing_file' did not resolve from sources" == str(e)


def test_include_duplicates(hub):
    with pytest.raises(IdemRunException) as e:
        run_sls(["include_duplicates"])
        assert "Duplicate sls declarations found: duplicate_name" == str(e)


def test_duplicates(hub):
    with pytest.raises(rend.exc.RenderException) as e:
        run_sls(["duplicates"])
        assert (
            "RenderException: Yaml render error: found conflicting ID 'duplicate_name'"
            == str(e)
        )


def test_include_different_sources(hub):
    ret = run_sls(["include_multi_sources"])
    assert ret
    assert len(ret) == 5, f"Expecting 5 states: {ret}"
    state_ref = "test_|-{name}_|-{name}_|-nop"
    for state_ref, state in ret.items():
        assert state["result"] is True, state_ref
    assert state_ref.format(name="test1_a") in ret
    assert state_ref.format(name="test1_b") in ret
    assert state_ref.format(name="test2_a") in ret
    assert state_ref.format(name="test2_b") in ret
    assert state_ref.format(name="wow") in ret
