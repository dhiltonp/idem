import datetime
import unittest.mock as mock
import uuid


async def test_empty(hub):
    # Test the results of a batch run with the minimum arguments and no states
    name = str(uuid.uuid4())

    await hub.idem.state.batch(name=name, states={})

    # Verify that everything was cleaned up
    ret = await hub.idem.state.status(name=name)
    assert ret == {
        "acct_profile": "default",
        "errors": [],
        "running": {},
        "status": 0,
        "status_name": "FINISHED",
        "test": False,
    }


async def test_undefined(hub):
    error_log = mock.MagicMock()
    hub.log.error = error_log
    ret = await hub.idem.state.status(name="undefined")
    assert ret == {
        "acct_profile": "",
        "errors": [],
        "running": {},
        "status": -4,
        "status_name": "UNDEFINED",
        "test": None,
    }
    error_log.assert_called_once_with("No idem run with Job ID: undefined")


async def test_finished(hub, event_loop):
    name = str(uuid.uuid4())

    with mock.patch("datetime.datetime", wraps=datetime.datetime) as dt:
        dt.now.return_value = datetime.datetime(1991, 3, 12)
        # Run states defined within a dictionary in code
        task = event_loop.create_task(
            hub.idem.state.batch(
                name=name,
                states={
                    "state name": {
                        "test.succeed_without_changes": [
                            {"name": "name", "resource_name": "name"}
                        ]
                    }
                },
                test=False,
            )
        )

        # Wait for the task to start
        await hub.pop.loop.sleep(1)

        # Get the current status of the state run
        ret = await hub.idem.state.status(name=name)

    # Verify the return results of the running states
    assert ret == {
        "acct_profile": "default",
        "errors": [],
        "running": {
            "test_|-state name_|-name_|-succeed_without_changes": {
                "__run_num": 1,
                "changes": {},
                "comment": "Success!",
                "esm_tag": "test_|-state " "name_|-name_|-",
                "name": "name",
                "new_state": None,
                "old_state": None,
                "rerun_data": None,
                "result": True,
                "sls_meta": {"ID_DECS": {}, "SLS": {}},
                "start_time": "1991-03-12 " "00:00:00",
                "__id__": "state name",
                "tag": "test_|-state " "name_|-name_|-succeed_without_changes",
                "total_seconds": 0.0,
                "ref": "states.test.succeed_without_changes",
                "acct_details": {},
            }
        },
        # TODO SHOULD be 0 and finished
        "status": 4,
        "status_name": "RUNNING",
        "test": False,
    }

    await task


async def test_encrypted_profiles(hub, event_loop):
    # Test the results of a batch run with the minimum arguments and no states
    name = str(uuid.uuid4())
    acct_key = hub.crypto.fernet.generate_key()
    profiles = {"provider": {"profile": {"kw1": "v1"}}}
    encrypted_profiles = hub.crypto.fernet.encrypt(profiles, key=acct_key)

    with mock.patch("datetime.datetime", wraps=datetime.datetime) as dt:
        dt.now.return_value = datetime.datetime(1991, 3, 12)
        task = event_loop.create_task(
            hub.idem.state.batch(
                name=name,
                states={
                    "state name": {
                        "test.succeed_without_changes": [
                            {"name": "name", "resource_name": "name"}
                        ]
                    }
                },
                encrypted_profiles=encrypted_profiles,
                acct_key=acct_key,
                default_acct_profile="profile",
                esm_plugin="null",
            )
        )

        # Wait for the task to start
        await hub.pop.loop.sleep(1)

        # Verify that everything was cleaned up
        ret = await hub.idem.state.status(name=name)

    assert (
        ret
        == {
            "acct_profile": "profile",
            "errors": [],
            "running": {
                "test_|-state name_|-name_|-succeed_without_changes": {
                    "__run_num": 1,
                    "changes": {},
                    "comment": "Success!",
                    "esm_tag": "test_|-state " "name_|-name_|-",
                    "name": "name",
                    "new_state": None,
                    "old_state": None,
                    "rerun_data": None,
                    "result": True,
                    "sls_meta": {"ID_DECS": {}, "SLS": {}},
                    "start_time": "1991-03-12 " "00:00:00",
                    "__id__": "state name",
                    "tag": "test_|-state " "name_|-name_|-succeed_without_changes",
                    "total_seconds": 0.0,
                    "ref": "states.test.succeed_without_changes",
                    "acct_details": {},
                }
            },
            # TODO should be 0 and FINISHED
            "status": 4,
            "status_name": "RUNNING",
            "test": False,
        }
        != {
            "acct_profile": "",
            "errors": [],
            "running": {},
            "status": -4,
            "status_name": "UNDEFINED",
            "test": None,
        }
    )

    await task
