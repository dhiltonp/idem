state:
    name: Some State
locations:
    - eastus
    - westus
empty:
vpc_resource_id: dummy
include:
    - included_params
nested_list:
    eu_locations:
        - eu-west
        - eu-north
