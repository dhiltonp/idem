first_thing:
  test.present:
    - resource_name: my-resource
    - old_state:
        public: public-data-old
        secret: secret-data-old
    - new_state:
        public: public-data
        secret: secret-data
    - sensitive:
      - secret


second_thing:
  test.present:
    - resource_name: my-resource-2
    - old_state:
        public: public-data-old
        secret: secret-data-old
    - new_state:
        public: public-data
        secret: secret-data
    - sensitive:
      - non-exist
