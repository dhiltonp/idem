second:
  test.nop:
  - require:
    - test: first

third:
  test.nop:
  - require:
    - test: second

any_order:
  test.nop

first:
  test.nop


test_invert_state_1:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        resource_id: idem-test-1
    - result: true

test_invert_state_2:
  test.present:
    - resource_id: idem-test-2
    - new_state:
        group_id: ${test:test_invert_state_1:resource_id}
        resource_id: idem-test-2
    - old_state:
        group_id: ${test:test_invert_state_1:resource_id}
        resource_id: idem-test-2
    - result: true
