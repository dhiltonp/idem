test_1:
    test_resource.present:
        - name: test_1
        - group: group_1

test_2_invalid_require_format:
    test_resource.present:
        - name: test_2
        - require:
            - test_resource.present: test_1
