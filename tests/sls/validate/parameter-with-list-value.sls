{% set org_id = params.get('org_id', ['ou-abc', 'ou-xzy', 'ou-pqr', 'ou-uvw']) %}
The Org policy:
  test.state.present:
  - org_ids: {{org_id}}
  - description: SCP tags policy
  - policy_type: TAG_POLICY
