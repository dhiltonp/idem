service1:
  sls.run:
    - sls_sources:
      - "./sls/include_relative_paths.sls"
    - params:
        - params.file1
    - db_parameter_group_name: service1_db_parameter_group
