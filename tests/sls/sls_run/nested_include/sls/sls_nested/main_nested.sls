test_nested_include_main_inn:
  test.present:
    - resource_id: test_nested_include_main
    - new_state:
        resource_id: test_nested_include_main
        arg_bind_ref: nested_val_1
    - result: True

test_nested_sls.run_with_include:
  sls.run:
      - sls_sources:
          - "./main_nested_include.sls"
      - resource_id: test_nested_include_main
      - new_state:
         resource_id: test_nested_include_main
         arg_bind_ref: nested_val_1
      - result: True
