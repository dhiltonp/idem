{% set nested_params = params.get("nested_params") %}

{% for nested_param in nested_params %}
test_nested_state_{{ nested_param }}:
  test.present:
    - resource_name: {{ nested_param }}
    - resource_id: {{ nested_params[nested_param] }}
    - new_state:
        resource_id: {{ nested_params[nested_param] }}
        name: {{ nested_param }}
{% endfor %}
