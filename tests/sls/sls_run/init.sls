service1:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file1
        - params.file_12
    - db_parameter_group_name: service1_db_parameter_group

service2:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file2
    - db_parameter_group_name: service2_db_parameter_group

service3:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file3
    - db_parameter_group_name: service3_db_parameter_group


first thing:
  test.succeed_with_changes


multi_result_new_state_1:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id_1": "resource-1"
        "name": "subnet1"
        "resource_id_2": "resource-2"
        "resource_id_3": "resource-3"


arg_bind_sls_run:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id": ${test:service1.test_include_sls_run_1:resource_id}
        "name": ${test:service1.test_include_sls_run_2:resource_id}
        "nested_run_1": ${test:service1.service4.test_include_sls_run_3:key}
        "nested_run_2": ${test:service2.service4.test_include_sls_run_3:key}
        "nested_run_3": ${test:service3.service4.test_include_sls_run_3:key}
    - require:
        - sls: service1
