# Reconcile with requirements:
# change_1 - require
# change_2 - arg_bind
# change_3 - same arg_bind

change_1:
  test.succeed_with_changes_with_rerun_data:
    - require:
        - test: good

good:
  test.succeed_without_changes_with_rerun_data

change_2:
 test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - first thing:
          - testing:new: parameters:test

change_3:
 test.succeed_with_arg_bind:
    - arg_bind:
      - test:
        - first thing:
          - testing:new: parameters:test

first thing:
  test.succeed_with_changes_with_rerun_data
