test_dataclass:
  test_dataclass.present:
    - arg_dc_flat:
        ArgWithNone: value
        arg_inner:
          InnerBoolTrue: True
          InnerBoolFalse: True
    - arg_dc_list:
        - ArgWithBoolTrue: True
        - ArgInt: 3
          ArgWithBoolFalse: True

test_dataclass_only_inner:
  test_dataclass.present:
    - arg_dc_flat:
        arg_inner:
          InnerInt: 3

test_dataclass_inner_tree:
  test_dataclass.present:
    - arg_dc_default_in_inner:
        arg_inner:
          InnerNone: InnerInt_default_20
        arg_inner_different_default:
          InnerNone: InnerInt_default_40
