state_A:
  test.present:
    - new_state:
        key: state_a_name
state_B:
  test.present:
    - resource_name: ${test:state_A:key}
    - new_state:
        key: ${test:state_A:key}

state_C:
  test.present:
    - new_state:
        key: ${test:state_B:key}
