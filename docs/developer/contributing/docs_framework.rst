=======================
Documentation Framework
=======================

Idem documentation is organized into user (topics) and developer sections. In general, document pages that include Python code belong in the developer section.

Within the user and developer sections, pages appear in one of four categories. When you're contributing to documentation, material might overlap between categories, but try to choose the closest match.

.. image:: ../../_static/img/quadrants.png
   :align: center
   :alt: Tutorials, How-to Guides, Explanation, Reference

For more about the categories, visit `Diátaxis <https://diataxis.fr>`_.
