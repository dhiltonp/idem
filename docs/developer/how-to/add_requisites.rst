=================
Adding Requisites
=================

Requisites are a basic language feature of idem, they allow for a definition of
how to perform a dependency check. The simplest requisite is ``require``. The ``require``
requisite simply mandates that the required ref has been processed and returned a
result of ``True``.

There are 2 plugin subsystems that are used in the resolution of requisites. The
``idem.req`` sub and the ``idem.rules`` sub. These are simple subs that allow the definition
of the requisite as well as the rules that create the requisite check.

An ``idem.req`` plugin has a function called ``define`` which is used to define what rules
are run by the requisite and how those rules are checked. This allows the rules to be
re-usable for multiple requsite definitions. The simple example of the ``require``
requisite is this:

.. code-block:: python

    def define(hub):
        """
        Return the definition used by the runtime to insert the conditions of the
        given requisite
        """
        return {
            "result": [True, None],
        }

This return states that only one rule needs to be checked, the ``result`` rule. The value,
in this case, that is passed to the result rule is the list ``[True, None]``. This
value is loaded into the ``result`` rule as the ``condition`` argument.

The rules are a little more complicated. They need to do the work to verify that for
all of the required ID refs that the rule is followed. Here is the ``result`` rule:

.. code-block:: python

    def check(hub, condition, reqret, chunk):
        """
        Check to see if the result is True
        """
        if isinstance(condition, list):
            if reqret["ret"]["result"] in condition:
                return {}
        if reqret["ret"]["result"] is condition:
            return {}
        else:
            return {
                "errors": [
                    f'Result of require {reqret["r_tag"]} is "{reqret["ret"]["result"]}", not "{condition}"'
                ]
            }

A rule plugin needs a function called ``check`` that is used to check the rule. It takes
3 arguments. These arguments are ``condition``, ``reqret``, and ``chunk``. These are objects that
are internal to idem. The ``condition`` is the data passed in by the ``define`` function
in the ``idem.req`` plugin. The ``reqret`` is the return information for a single required
ID that has already been executed. The ``chunk`` is the ID declaration with the assigned
requisite.
The return from this function defines
