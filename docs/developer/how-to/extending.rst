==============
Extending Idem
==============

Extending Idem is simple, but it does require a few steps. To extend
Idem you need to create a new Idem plugin project using *POP*. Now don't run
away, this has been designed to be easy!

What is POP?
============

You don't need to understand the inner workings of Plugin Oriented Programming (POP) to extend Idem, just think about it as a system for writing and
managing plugins. Idem is all about plugins!

If you want to learn more about the details of POP, take a look at the docs.
It is powerful stuff and might change how you program forever:
https://pop.readthedocs.io

Let's Get Down to Business
==========================

Start by installing Idem:

.. code-block:: bash

    $ pip install idem

This will download and install both ``idem`` and ``pop``. Now you can start your
project by calling ``pop-create`` to make the structure you need:

.. code-block:: bash

    $ pop-create idem_tester -t v -d exec states

By passing ``-t v`` to ``pop-create`` we are telling ``pop-create`` that this is a
*Vertical App Merge* project. By passing ``-d exec states`` we are asking
``pop-create`` to add the two dynamic names ``exec`` and ``states`` to the project.

This will create a new project called ``idem_tester`` with everything you need
to get the ball rolling.

Making Your First Idem State
============================

In your new project there will be a directory called ``idem_tester/states``, in
this directory add a file called ``trial.py``:

.. code-block:: python

    async def run(hub, ctx, name):
        """
        Do a simple trial run
        """
        return {
            "name": name,
            "result": True,
            "changes": {},
            "comment": "It Ran!",
        }

For idem to run, *states* functions need to return a python dict that has four fields,
``name``, ``result``, ``changes``, and ``comment``. These fields are used by Idem to not
only expose data to the user, but also to track the internal execution of the system.

Next install your new project. For ``idem`` to be able to use it your project, it
needs to be in the python path. There are a lot of convenient ways to manage the
installation and deployment of POP projects, but for now we can just use good
old ``pip``:

.. code-block:: bash

    $ pip install -e /path/to/your/project/root

Now you can execute a state with ``idem``. As you will see, ``pop`` and ``idem`` are
all about hierarchical code. Idem runs code out of a directory, so you need to
point Idem to a directory that contains structured layer state (SLS) files. Go ahead and ``cd`` to
another directory and make a new directory for SLS files.

.. code-block:: bash

    $ mkdir try
    $ cd try

Now open a file called ``try.sls``:

.. code-block:: yaml

    try something:
        trial.run

Now from that directory run idem:

.. code-block:: bash

    $ idem --sls try

And you will see the results from running your trial.run state!
