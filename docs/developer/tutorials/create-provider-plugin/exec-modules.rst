============
Exec Modules
============

An Exec Module provides imperative functions to directly interact with your
cloud or API. All python functions in an Exec module will be made available on the CLI
at `exec.<your module>.<function name>`. These functions should
imperatively take an action such as creating something or deleting something.

You can create as many nested directories to group functions as needed. For
example, an Idem exec function for a cloud might have `exec.<my
cloud>.network.vpc.create`. So all functions related to networking within the
cloud are in the network directory or file under the `my cloud` directory. In
this case "network" is the sub-directory and "vpc" is the file.

----

Create Sqlite Exec module
=========================

We're going to add a simple Idem Exec module to our idem-sqlite-demo Provider plugin
to interact with a small sqlite database. We'll be using python functions to
interact with this sqlite database, but the same principles hold if you're
communicating with your cloud's API through a python module or directly making
http requests to the API.

Create Sqlite module
++++++++++++++++++++

Our `idem_sqlite_demo/exec/demo` directory should already exist. Let's create our
sqlite python module.

.. code-block:: bash

    touch idem_sqlite_demo/exec/demo/sqlite.py

----


Add Setup function
++++++++++++++++++

.. note::
   In a production provider plugin, a separate "db" module to handle creating
   and managing the sqlite db file would be recommended. In the interest of
   brevity, we're just going to have our sqlite module handle setting up the
   database for us.

Edit `idem_sqlite_demo/exec/demo/sqlite.py` with the following contents

.. literalinclude:: exec-modules_files/setup_db-step1.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py

----

We'll use this function to set up our sqlite database. We'll specify a default
location, but you can specify a different path if you desire.

Let's initialize our database.

.. code-block:: bash

    idem exec demo.sqlite.setup_db

You should see this output:

.. code-block:: text

    |_
      - 1
      - Sarah
      - 28
    |_
      - 2
      - David
      - 45

----

Add List function
+++++++++++++++++

Now we're going to create a list function to list all our database entries. You
would do something similar to list all VMs in your cloud or all the VPCs.

See `idem-aws exec functions
<https://gitlab.com/vmware/idem/idem-aws/-/tree/master/idem_aws/exec/aws>`_ for
examples.

First we need to add a function alias at the top of our sqlite.py file. The
word `list` is a reserved word in python so we need to define our function as
`list_`. Notice the underscore. This will allow us to use `list` on the CLI and
when calling the function directly, but not overwrite the built in Python
`list`.

Make the top of your file look like this:

.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :lines: 1-5


Now let's add our list function. We'll add this above the existing `setup_db`
function. Notice the underscore in the function definition. The
`__funct_alias__` above remaps that to `list`.


.. literalinclude:: exec-modules_files/list_function-step2.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py

Let's list our database entries:

.. code-block:: bash

    idem exec demo.sqlite.list

We should get the following result:

.. code-block:: text

    |_
      - 1
      - Sarah
      - 28
    |_
      - 2
      - David
      - 45

----

I mentioned "Tool plugins" in the Plugin overview. We're going to create our
first utility function now. This function is going to reformat the sqlite
return. We'll make this a Tool function so that we can easily reuse this
elsewhere.

First we'll create the demo directory in the tool subdirectory and create the
sqlite file:

.. code-block:: bash

    mkdir idem_sqlite_demo/tool/demo
    touch idem_sqlite_demo/tool/demo/sqlite.py

Now add the following contents to your sqlite file:

.. literalinclude:: exec-modules_files/tool_function-step3.py
   :language: python
   :caption: idem_sqlite_demo/tool/demo/sqlite.py


Now we can use this function to change the format of the sqlite return anywhere
we want. If we decide to change the output we can make that change in this one
location.

Here's the updated `list` function:

.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :pyobject: list_
   :emphasize-lines: 21-22



Let's list our database entries again:

.. code-block:: bash

    idem exec demo.sqlite.list

We should now get the following result that includes the column names:

.. code-block:: text

    |_
      ----------
      resource_id:
          1
      name:
          Sarah
      age:
          28
    |_
      ----------
      resource_id:
          2
      name:
          David
      age:
          45


Let's update the setup_db function to also use our new sqlite Tool function:

Update your setup_db function to look like this:

.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :pyobject: setup_db
   :emphasize-lines: 42-43



----

Add Get function
++++++++++++++++

Now we're going to create a `get` function that will retrieve information about
a specific entry in our sqlite database. This would be similar to requesting
information about a specific VM from our cloud.

Add the following function to your file at `idem_sqlite_demo/exec/demo/sqlite.py`. You
can put this above or below your `list_` function.


.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :pyobject: get



In this `get` function we have a required position argument `resource_id`. We
then query for `resource_id` in our database and return the results. Use a
resource_id from one of the entries we viewed by using the `list` function.

Let's run our get command.


.. code-block:: bash

    idem exec demo.sqlite.get 2

We should see the following result:

.. code-block:: bash

    resource_id:
        2
    name:
        David
    age:
        45

----

Add Create function
+++++++++++++++++++

Now let's add a `create` function. This function will take a name and age as
arguments and add them to the database. In the interest of brevity we're not
going to add functionality for maintaining uniqueness in this database. The
reader can add this functionality if desired.

Add this create function anywhere in the sqlite.py file we've been working on.

.. literalinclude:: exec-modules_files/create_function-step4.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py


Now we can use our new `create` function to add a new entry to our database.

.. code-block:: bash

    idem exec demo.sqlite.create 'Joe' 22

You should see output of just `None`.

.. code-block:: bash

    None

----

We're not going to spend too much time perfecting this example, but just seeing
`None` doesn't leave me confident that our data successfully made it into our
database. Let's query the database for our info and return that. Instead of
creating a new sql query, we'll reuse our `get` function that we've already
added. This will introduce us into how we can use Idem Exec functions from
within any Idem function.

You might have noticed that all the functions we've created so far have had
`hub` as their first argument. The `hub` is a `pop` feature. You can dive into
the details of `pop's hub here.
<https://pop.readthedocs.io/en/latest/topics/hub_overview.html>`_

The quick explanation is that all of the functions we've created so far are
available inside ANY Idem function at `hub.exec.demo.sqlite.<FUNCTION NAME>`.
So in this case we're going to use `hub.exec.demo.sqlite.get` to query our
database.

Here's our updated sqlite.py:

.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :pyobject: create



Let's try running create again.


.. code-block:: bash

    idem exec demo.sqlite.create 'Sonya' 23

You should see that our data was entered and then queried from out database. We
were able to reuse our `get` function we already created.

.. code-block:: bash

    resource_id:
        5
    name:
        Sonya
    age:
        23

----

Add Update function
+++++++++++++++++++

Finally, let's add an `update` function. This function will require the
`resource_id` and optionally any of the data that we want to update.

.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
   :caption: idem_sqlite_demo/exec/demo/sqlite.py
   :pyobject: update

Let's try updating our previous entry.


.. code-block:: bash

    idem exec demo.sqlite.update resource_id=5 age=24

You should see that Sonya's age was updated.

.. code-block:: bash

    resource_id:
        5
    name:
        Sonya
    age:
        24

----

Complete Sqlite Exec module
===========================

For the sake of completeness here's the complete sqlite Exec module, including
a `delete` function. Note that I've added detailed docstrings for each
function. Idem uses this information for presenting documentation to the user.
The Idem project uses `Google style
<https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings>`_
docstrings as a standard.


.. literalinclude:: exec-modules_files/sqlite.py
   :language: python
