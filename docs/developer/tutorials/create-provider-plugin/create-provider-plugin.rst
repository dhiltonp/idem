======================
Create Provider Plugin
======================


It is recommended to use a python virtualenv when creating a new Idem provider
plugin. We'll give some instructions below, but feel free to use what
environment tool you're comfortable with.

Create a virtual env
====================

.. warning::

   Ensure you have Python 3.8 or later available. You're good if running the
   command `python -V` displays a version of Python 3.8 or newer. Otherwise you
   might need to use `python3` instead of `python` in the commands in the rest
   of this tutorial.

We're going to create the directory where we'll begin our project. We're
going to use the project name "idem-sqlite-demo".

.. code-block:: bash

    mkdir idem-sqlite-demo
    cd idem-sqlite-demo
    python -m venv env
    source env/bin/activate

Let's update to the latest pip inside our virtual env:

.. code-block:: bash

    pip install -U pip


Now you should be in your new python virtualenv.

----

Install Idem and pop-create
+++++++++++++++++++++++++++

Now we're going to install "pop-create".

.. code-block:: bash

    pip install idem
    pip install pop-create

The above command installs everything we need to create our provider plugin.

----

Initiate Plugin
+++++++++++++++

The following command creates our plugin. The `-n` flag specifies the name of
our plugin. The `-tv` flag indicates the type is a "vertical app merge
project". This is a POP term that means our plugin is meant to be an extension
to local Idem command. The `-d` flag accepts a list names of "Dynamic
Namespaces" that you want to have created for you.


.. code-block:: bash

    pop-create -n idem-sqlite-demo -tv -d exec states tool acct


Now we have the skeleton of our first Idem Provider Plugin. You'll notice
that things such as docs and testing has already been set up for you.

Install New Plugin
++++++++++++++++++

Now let's install our new Idem Provider Plugin into our environment.

.. code-block:: bash

    pip install -e .

.. warning::

    There is currently a bug introduced in setuptools 64.0.0 that makes editable
    installs fail. If your version of setuptools 64.0.0 or greater you may need to
    use the following command to workaround this bug.

    .. code-block:: bash

        pip install -e . --config-settings editable_mode=strict

Create a Demo function
======================

Create test.py
++++++++++++++

Now that we have the skeleton of an Idem Provider Plugin, let's create a simple
Exec module to show how it works.

.. code-block:: bash

    mkdir idem_sqlite_demo/exec/demo
    touch idem_sqlite_demo/exec/demo/test.py


Edit `idem_sqlite_demo/exec/demo/test.py` with the following contents

.. code-block:: python

    async def hello(hub, ctx):
        """hello world example"""
        result = dict(comment=[], ret=None, result=True)
        result["comment"] = "sample comment"
        result["ret"] = "Hello, World."
        return result

Run test.py
+++++++++++

Now we can run our little `hello` function with Idem like this:


.. code-block:: bash

    idem exec demo.test.hello


Notice that `demo` is the directory inside our `exec` directory, `test` is the
python file `test.py` and `hello` is the python function `hello` inside th
`test.py` file. You can always track down the location of code within Idem like
that. You can nest directories as deep as you need. If your cloud has multiple
products you can group them by file and directory however you need. A good
example of this is the `idem-aws exec module
<https://gitlab.com/vmware/idem/idem-aws/-/tree/master/idem_aws/exec/aws>`_.

Now that we've created a simple Hellow World example, let's go a little deeper
in the next section on :doc:`Exec modules <exec-modules>`.
