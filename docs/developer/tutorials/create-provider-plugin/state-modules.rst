=============
State Modules
=============

State modules provide idempotent functions to configure your cloud. State
modules generally use functions from related Exec modules to check for the
current status of resources and make the necessary changes.

----

Common State Module Functions
=============================

There are two functions that are generally used for Idem State functions,
``present`` and ``absent``.

----

Present State Function
++++++++++++++++++++++

The ``present`` state function will ensure that your resource exists.

The ``present`` function will use the related ``get`` or ``list`` Exec function
to determine if the resource already exists or not. If the resource already
exists, then it will return immediately with a successful response. If the
resource doesn't exist, it will use the corresponding Exec function to create
the missing resource and then return a successful response if the create
function succeeds.


Absent State Function
+++++++++++++++++++++

In a similar manner the ``absent`` function will ensure that your specified
resource does not exist.

It will use the related ``get`` or ``list`` function to determine if the
resource exists or not. If the resource doesn't exist the function will return
immediately with a successful status. If the resource does exist the function
will use the related Exec module ``delete`` function to remove the resource.


----


State Modules and SLS Files
===========================

Generally, you will define how you want your infrastructure configured in yaml
files with an ``.sls`` extension. These are often refered to as ``sls files``
or ``state files``. These ``sls files`` will contain yaml datastructures that
will use your Idem State Functions to define how your infrastructure is
configured.

Here is an example of an sls file that configures an AWS VPC:

.. code-block:: yaml
   :caption: ~/my_idem_files/my_vpc.sls

    vpc-07fa1462:
      aws.ec2.vpc.present:
      - name: vpc-07fa1462
      - resource_id: vpc-07fa1462
      - instance_tenancy: default
      - cidr_block_association_set:
        - AssociationId: vpc-cidr-assoc-b2ba2fdb
          CidrBlock: 172.31.0.0/16
          CidrBlockState:
            State: associated
      - enable_dns_hostnames: true
      - enable_dns_support: true

Now we can apply this sls file in an idempotent operation like this:

.. code-block:: shell

    idem state ~/my_idem_files/my_vpc.sls

----

Create Present State Function
=============================

Let's create our ``present`` state function for our ``idem-sqlite-demo``
provider plugin.

.. code-block:: shell

    mkdir idem_sqlite_demo/states/demo
    touch idem_sqlite_demo/states/demo/sqlite.py
