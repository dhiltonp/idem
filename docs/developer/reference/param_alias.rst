=====================
SLS Parameter Aliases
=====================

Param aliases provide a convenient way to use alternative names for parameters in SLS files.
They can make your SLS files more readable and easier to understand by allowing you to use more descriptive or familiar
names for parameters.


Function Definition with Param Aliases
--------------------------------------

To use param aliases in an SLS file, you'll first need to define them in the corresponding Python function definition
located in the "states" directory. To do this, include the desired alias in the type hint of the function signature,
following the syntax: ("alias=ALIAS_NAME", TYPE).

This is particularly useful when python keywords are used as parameter names.

Here's an example function definition with a param alias:

.. code-block:: python

    async def present(hub, ctx, name: str, zone: str, project: ("alias=id", str) = None):
        ...


In this example, the ``project`` parameter has an alias called ``id`` which is a python keyword.


Using Param Aliases in SLS Files
--------------------------------

To use the param alias in an SLS file, you can replace the original parameter name with the alias. The following
examples demonstrate how to use both the original parameter name and its alias in an SLS file:

Example 1: Using the original parameter name

.. code-block:: sls

    my_instance:
      gcp.compute.instance.present:
        - zone: ...
        - project: ...

Example 2: Using the alias

.. code-block:: sls

    my_instance:
      gcp.compute.instance.present:
        - zone: ...
        - id: ...

Both of these examples are equivalent and will produce the same result when executed.  Using param aliases can simplify
your SLS files, improve readability, and make it easier for others to understand the purpose of each parameter.
