===========================
Multiple Account Management
===========================

You can securely run Idem against multiple cloud accounts and providers. The Idem *acct* tool lets you specify cloud account and provider information in a file. The *acct* tool is a dependency of Idem. It is used to encrypt the file that stores the account information securely on the file system.

Support for file-based authentication was added as of Idem 6.
Additional authorization mechanisms are expected in future Idem releases.

Static Account Management
=========================

In this example, you create a file in which to store credentials. The file is a simple YAML file that can store credentials for multiple providers and accounts.

Run idem with the ``acct_edit`` subcommand to create a new encrypted credentials file.
It will open a new or existing encrypted credentials file in the OS's default text editor.
You can also specify an editor with the ``--editor`` flag.

.. code-block:: bash

    $ idem acct_edit creds.yaml

The following example ``creds.yml`` file includes sample aws system values. The only profile shown is an aws default profile, but you could have multiple sections with profiles for more providers and accounts.
Write these contents to the acct_file in your text editor then save and quit.

.. code-block:: yaml

    aws:
      default:
        aws_access_key_id:
        aws_secret_access_key:
        region_name:

After creating the file with the ``acct_edit`` subcommand, a key will be printed to the CLI if this was your first time.

.. code-block:: bash

    $ idem acct_edit creds.yml
    New encrypted file created at: creds.yml
    The file was encrypted with this key:
    j-ytfz45n2wRUHXZJsumtG5_Dih3b3lTA1P2apqNuFg=

Now you have an encrypted credentials file and a key to access it. Keep the key in a safe place.

To run ``idem`` with credentials stored in the file, use the ``--acct-file`` and ``--acct-key`` options.

In addition, you can use the ``--acct-profile`` option to select a profile from within a credentials file that contains multiple profiles. In the example above, ``default`` is the account profile.

If there are multiple profiles, and you don't supply the ``--acct-profile`` option, the ``default`` profile is used.

If you don't want to pass account information as CLI options, you can set the following environment variables:

.. code-block:: bash

    $ export ACCT_FILE="<full path to creds.yml.fernet>"
    $ export ACCT_KEY="<creds file encryption key>"


ACCT Render Pipes
=================

Before an acct_file is encrypted, it will be passed through the specified acct render pipes.
The default render pipe is "jinja|yaml"

.. code-block:: bash

    $ idem encrypt credentials.yaml --render-pipe="jinja|yaml"

Unencrypted ACCT File
=====================

If no ACCT_KEY is provided, then acct will assume that the ACCT_FILE is unencrypted.

For states/exec modules to specify a custom acct render pipe, it needs to be specified in the idem config file.

.. code-block:: yaml

    # idem-config.cfg
    acct:
      render_pipe: jinja|yaml

Allowed Backend Profiles
========================

If the idem config file specifies allowed_backend_profiles, then only backend profiles with names in this list will be processed by acct.
The default is to process ALL acct backend profiles.

The following config file shows 3 profiles that are allow-listed in the idem config file:

.. code-block:: yaml

    # idem-config.cfg
    acct:
      allowed_backend_profiles:
        - allowed_backend_profile_name_1
        - allowed_backend_profile_name_2
        - allowed_backend_profile_name_3

The following unencrypted credentials file has multiple profiles for account backends under the "vault" and "lastpass" providers.
Each ``acct-backend`` profile contains other normal profiles for acct to use.
For example, a vault acct-backend may connect to a vault data store that contains acct profiles for connecting to aws and azure.
The vault acct-backend profile contains credentials for connecting to vault.
The vault acct-backend plugin connects to vault and collects more credentials for idem projects from vault.
Only profile names that match the ``acct:allowed_backend_profiles`` config option will be used to collect more credentials from the acct backend profiles.
This way, a user can be selective about which acct-backend to use in the case of conflicts.

.. code-block:: yaml

    # credentials.yaml
    acct-backends:
      vault:
        allowed_backend_profile_name_1:
          kwarg1: val1
        allowed_backend_profile_name_2:
          kwarg1: val1
        allowed_backend_profile_name_3:
          kwarg1: val1
        defined_but_not_allowed_profile:
          kwarg1: val1
      lastpass:
        allowed_backend_profile_name_1:
          kwarg1: val1
        defined_but_not_allowed_profile:
          kwarg1: val1

.. code-block:: bash

    $ idem exec test.ping --acct-file=credentials.yaml --config=idem-config.cfg

ACCT Serial Plugin
==================

The ``pop-serial`` plugin that is used by acct to serialize acct data before it is encrypted can be specified in the idem config file.
The default plugin for serializing data in acct is ``msgpack``:

.. code-block:: yaml

    # idem-config.cfg
    acct:
      serial_plugin: msgpack
