
.. _reconciliation_loop:

===================
Reconciliation Loop
===================

In Idem, a reconciliation loop reapplies states until all states are successfully realized. A reconciliation loop reapplies states that are marked as pending and stops when no more states are pending.

Reconciliation may also stop when no further progress is possible, such as when the maximum number of allowable loops is reached.

Reconciler Plugin
=================

By default, reconciliation uses the ``basic`` reconciler plugin that comes with Idem. The plugin uses a method similar to the following:

.. code-block:: python

    from typing import Dict, Any


    async def loop(
        hub,
        pending_plugin: str,
        name: str,
        apply_kwargs: Dict[str, Any],
    ):
        ...

The reconciler should return a dictionary similar to the following:

.. code-block:: python

    {
        "re_runs_count": "<number reconciliation loop iterations>",
    }

Pending Plugin
==============

The pending plugin determines whether a state is pending and in need of reconciliation. A returned value of *True* means the state is pending and needs to be reapplied.

In Idem, the default implementation is defined in ``default.py`` and has the following functionality:

* If a state has a customized ``is_pending`` method, that method is repeatedly called until the state is either no longer pending or ``max-pending-reruns`` is reached. The ``max-pending-reruns`` option defaults to 600 and helps you avoid infinite iterations by limiting reconciliation attempts.

* For states without a custom ``is_pending`` method, the pending plugin returns *True* if ``result=False`` or there is "rerun_data" in the context. In these cases, the pending plugin marks the state as non-pending after 3 consecutive loops that produce the same result and 'changes'. And the total runs will not exceed ``max-pending-reruns``.

.. note::

    Other plugins might override the default pending behavior.

Specifying Plugins at the CLI
=============================

You can specify the reconciler and pending plugins as ``idem state`` command arguments:

.. code-block:: text

    --reconciler=basic | -R=basic | -r=basic
    --pending=default | -P=default | -p=default

The reconciliation loop is enabled by default. To disable it, use:

.. code-block:: text

    --reconciler=none

To set the maximum number of reconciliation loops for states with a customized ``is_pending`` method, use:

.. code-block:: text

    --max-pending-reruns { default = 600 }

Notes
=====

* Reconciliation doesn't apply to ``exec.run`` commands that are successful.

* Reconciliation doesn't run during test execution (``--test=True``).

* :ref:`Click here<customize_reconciliation>` for more information about reconciliation loops.
