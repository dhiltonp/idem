.. _argument_binding:

================
Argument Binding
================

An argument binding reference sets the state definition argument value to the result of another state execution. In this way, argument binding references determine the order of state execution in the structured layer state (SLS) file structure.

An argument binding reference uses the following format:

.. code-block:: text

    ${<cloud>:<state>:<property_path>}

Where <cloud> is the state cloud path reference (excluding function reference), <state> is the state declaration ID,
and <property_path> is a colon (:) delimited path to the property value.

If the same state is created multiple times with different resource names, the format should contain the resource_name as following:

.. code-block:: text

    ${<cloud>:<state>[<resource_name>]:<property_path>}

In the following example, State_B will be executed before State_A because the State_A argument ``state_B_id`` requires the ``ID`` value from State_B output.

.. code-block:: sls

   State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_id: "${cloud:State_B:ID}"
    State_B:
      cloud.instance.present:
        - name: "Instance B"


Indexes
=======

An argument binding reference can contain an index to point to a specific element of a collection property, as shown in the following example.

.. code-block:: sls

    State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_address: "${cloud:State_B:nics[0]:address}"
    State_B:
      cloud.instance.present:
        - name: "Instance B"
        - nics:
            - network_name: "Network_1"
              # address is populated after state is executed
              address:
            - network_name: "Network_2"
              # address is populated after state is executed
              address:

An argument binding reference can contain a wildcard (*) index to collect all elements in a collection property.
In the following example, State_A ``state_B_addresses`` argument will be set to a list of two addresses, one address for each NIC of State_B.


.. code-block:: sls

    State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_addresses: "${cloud:State_B:nics[*]:address}"
    State_B:
      cloud.instance.present:
        - name: "Instance B"
        - nics:
            - network_name: "Network_1"
              # address is populated after state is executed
              address:
            - network_name: "Network_2"
              # address is populated after state is executed
              address:

Resource Contract
=================

To support argument binding, a cloud plugin must implement a ``resource`` contract, where every state execution function must return a ``new_state`` property as part of the return dictionary. The ``new_state`` is used to resolve argument binding requisites.

Arg_bind Requisites
===================

Behind-the-scenes argument binding references are implemented using the Idem requisite system, where argument binding references are parsed during the SLS compilation phase and added to high data as arg_bind requisites. During arg_bind requisite execution, the ``new_state`` property returned after function execution is used to resolve the value of the referenced parameter.

The following example demonstrates SLS high data after the compilation phase, where ``${cloud:State_B:ID}`` is resolved as the arg_bind requisite.

.. code-block:: sls

   State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_id: "${cloud:State_B:ID}"
        - arg_bind:
          - cloud:
             - State_B
                - ID: state_B_id
    State_B:
      cloud.instance.present:
        - name: "Instance B"
