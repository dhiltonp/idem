============
"Run" States
============

The Idem ``sls.run`` state lets you invoke other SLS files multiple times but with different arguments and parameters each time. An ``sls.run`` state uses the following inputs:

* sls_sources(list[str])

  SLS files to invoke

* inline parameters

  Parameter values entered directly in the ``sls.run``, which are then passed to the invoked SLS files to create the different results

* params(list[str, Any], Optional)

  Parameter files to pass to the current execution of the ``sls.run`` state, where values from those files create the different results

In case of duplicate parameter names, the order of value precedence is:

1. Inline parameters entered directly as ``sls.run`` arguments

#. Parameters from parameter files included in a ``params`` argument of the ``sls.run`` state declaration

#. Parameter files supplied to ``idem`` execution, such as with the ``--params`` option from the command line

Example SLS Run
===============

The following example creates three networks with different options by passing parameters to ``sls.run`` state execution. Idem applies the differences by calling a separate parameter file in each case, and by using an inline parameter to assign different names.

.. code-block:: sls

    Network_A:
      sls.run:
        - sls_sources:
            - sls.network
        - network_name: Network_A
        - params:
            - params.file1

    Network_B:
      sls.run:
        - sls_sources:
            - sls.network
        - network_name: Network_B
        - params:
            - params.file2

    Network_C:
      sls.run:
        - sls_sources:
            - sls.network
        - network_name: Network_C
        - params:
            - params.file3

To add a fourth network, you would add another ``sls.run`` block with different parameters.

Nested SLS Runs
===============

An SLS source file called with the ``sls.run`` function can also include another ``sls.run`` block. Using this technique, you can nest ``sls.run`` blocks within different SLS files:

.. code-block:: sls

    State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_address: "${cloud:State_B:nics[0]:address}"

    State_B:
      cloud.instance.present:
        - name: "Instance B"
        - nics:
            - network_name: "Network_1"
              # address is populated after state is executed
              address:
            - network_name: "Network_2"
              # address is populated after state is executed
              address:

    # nested sls.run

    State_C:
      sls.run:
        - sls_sources:
            - sls.instances
        - params:
            - params.file3
        - instance_tag: cluster-autoscale

Argument Binding
================

To create an argument binding to a value that comes from a file included in an ``sls.run``, add ``<sls-run-state-name>.`` to the beginning of the state name. For example:

.. code-block:: sls

    ${cloud:Network_A.State_B:nics[0]:address}

Where

* ``sls:Network_A`` refers to the ``sls.run`` state

* ``cloud:State_B`` refers to the state to reference inside the included file

* ``nics[0]:address`` refers to the desired attribute in the referenced state

The following example shows two argument bindings, to the results from two ``sls.run`` states that reference the same included file and attribute.
when we do argument binding from ``sls.run`` state, we should add the require requisite of that ``sls.run`` state.

.. code-block:: sls

    arg_bind_sls_run:
      test.present:
        - result: True
        - changes: 'skip'
        - new_state:
            "network_A_address": ${cloud:Network_A.State_B:nics[0]:address}
            "network_B_address": ${cloud:Network_B.State_B:nics[0]:address}
        - require:
            - sls: Network_A
