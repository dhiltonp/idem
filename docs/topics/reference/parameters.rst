==============
SLS Parameters
==============

Writing and applying infrastructure as data relies on SLS files, where declarative data may be reapplied again and again to produce and maintain a desired result. In SLS files, parameters are how you describe and customize for the result that you want.

Creating a Parameter File
=========================

A parameter file is a special SLS file that only contains key-value pairs as shown in the following example. A parameter file doesn't include any state declarations.

.. code-block:: sls

        location: eastus
        subscription_id: xxx-xxxxxxxxxxxx
        locations:
            - eastus
            - westus

Parameter files can call other parameter files by using an ``include`` statement as shown in the following example.

.. code-block:: sls

        subscription_id: xxx-xxxxxxxxxxxx
        include:
            - params_extra

In the example, ``params_extra.sls`` then contains the following content.

.. code-block:: sls

        locations:
            - eastus
            - westus

Calling Parameters From a State
===============================

In SLS files with state declarations, parameters are available as a `Python dictionary object <https://docs.python.org/3/library/stdtypes.html#dict>`_ called ``params``.

Because ``params`` is a dictionary object, you can use dictionary functions such as `get <https://docs.python.org/3/library/stdtypes.html#dict.get>`_, `items <https://docs.python.org/3/library/stdtypes.html#dict.items>`_, and so on.

For example, you retrieve values with ``params.get('parameter')``
where ``parameter`` is the parameter name. Parameter values from the earlier example would be retrieved as shown here.

.. code-block:: sls

        {{ params.get['location'] }}
        {{ params['subscription_id'] }}
        {{ params.get['locations'][1] }}

Default Parameter Values
========================

To enforce a default value for the parameter, use ``params.get('param', 'default_value')`` where ``default_value`` is the value you want.

Missing Parameter Values
========================

If a called parameter is missing from the parameters file or has no value ``params.get('parameter')`` returns ``None`` as the result.

To verify that a parameter is defined in the parameters file, use ``params['missing_parameter']`` where ``missing_parameter`` is the one you're looking for. If the parameter isn't defined, an exception similar to the following occurs.

.. code-block:: text

    Jinja variable: 'idem.idem.idem.idem.state. object' has no attribute 'missing_parameter'

Running an SLS State File and Parameter File
============================================

In addition to defining parameters and referencing them within state files, commands need to specify the parameters to use.

To run a state file along with an associated parameter file, add the ``--params`` command line option.

.. code-block:: bash

    $ idem state my_state.sls --params path/to/parameter_file.sls

Running an SLS State File and Multiple Parameter Sources
========================================================

Multiple parameter sources are supported. Locations specified in ``--params`` reference locations in ``--param-sources`` where each source is searched in the order given.

.. code-block:: bash

    $ idem state my_state.sls --params "file.sls" "vault/location/specific" --param-sources "file://local/file.sls" "vault://vault/location"

In the preceding command, ``file.sls`` is successfully found in the first parameter source ``file://local/file.sls``.

Next, Idem checks for ``vault/location/specific`` in the first parameter source ``file://local/file.sls``.

It isn't there, so Idem then checks for ``vault/location/specific`` in the second parameter source ``vault://vault/location``.

All found sources are read and compiled into a single parameter tree.

Parameter Precedence
====================

Parameters can be overridden according to the following rules.

- A parameter value directly in a parameter file overrides a value coming from an included parameter file.

- In a parameter file with multiple included files, a value from a later included file overrides a value from an earlier one.

- In a command line that calls multiple parameter files, a parameter file from later in the command line overrides one given earlier.

See the following example, where ``param.sls`` is the parameter file called by the command line.

In this hierarchy of included parameters, ``a`` will be set to ``4`` and ``b`` will be set to ``4``:

.. code-block:: text

    ==> param.sls <==
    include:
    - param2

    ==> param2.sls <==
    include:
    - param3
    - param4

    ==> param3.sls <==
    a: 3
    b: 3

    ==> param4.sls <==
    a: 4
    b: 4

If you change the example so that ``param2.sls`` reverses the include order, parameter ``a`` will be ``3`` and parameter ``b`` will be ``3``.

.. code-block:: text

    ==> param2.sls <==
    include:
    - param4
    - param3

If you change the example so that ``param2.sls`` has its own assignment of ``2`` for parameter ``a``, ``a`` will be ``2`` and ``b`` will be ``3``.

.. code-block:: text

    ==> param2.sls <==
    include:
    - param4
    - param3
    a: 2
