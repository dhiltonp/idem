=============
Single Target
=============

The ``target`` CLI option allows you to specify a single state.
The resource will be executed with all its dependencies.
The target value is the declaration ID of the state, which
is guaranteed to be unique.
If there are multiple states under a single declaration ID
all of them will be invoked.

.. code-block:: text

    $ idem state <SLS> --target=<the_target>

Specifying an invalid target will result in an error.

For example, for the following SLS:

.. code-block:: sls

    resource_a:
      type1.present:
        - require:
          - resource_b

    resource_b:
      type2.present:
        - name: the_name_of_resource_b
        - require:
          - resource_c

    resource_c:
      type3.present

    grouped_resource_d:
      typeA.present:
        - require:
          - resource_c
      typeB.present:
        - name: typeB_resource
      typeC.present:
        - name: typeC_resource

.. code-block:: bash

    $ idem state SLS --target=resource_a

will result in resource_a, resource_b and resource_c.

.. code-block:: bash

    $ idem state SLS --target=grouped_resource_d

will result in the 3 nested resources (typeA, typeB and typeC) and resource_c.
