===================
SLS Directory Trees
===================

SLS files can be arranged in a tree fo easy maintenance and development. This allows you to
arrange your Idem code in a way that makes it easy to keep track of things, but also makes
it easy to modify other SLS files, make code reusable, and more!

When you run Idem and just give it a file to run, it assumes that your SLS tree is rooted in
that file's directory. This makes it easy to make multiple files that can be called together
while just starting from an easy to find entry point.

Lets assume that you run ``idem state start.sls`` in a directory called ``idem/``. Then you have
defined that the directory called ``idem/`` is the root of your SLS tree. Now all of the SLS
references inside ``include`` directives are references relative to the ``idem/`` directory.

SLS tree paths are resolved in one of two ways. An SLS reference of ``foo.bar`` will resolve
to either ``foo/bar.sls`` or to ``foo/bar/init.sls``. This allows for code to be easily
organized into directories that contain groupings of code.
