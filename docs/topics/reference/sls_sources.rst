=====================
Sources for SLS Files
=====================

SLS sources are directory trees, archives, and remote stores that contain SLS files.
SLS and param sources can come from many different places.
The plugins that can be used to process SLS sources are in idem/idem/sls.

The format for an sls sources is:

.. code-block:: text

    <protocol>://<resource>

The format for authenticated sls sources is:

.. code-block:: text

    <protocol_plugin>://<acct_profile>@<resource>

The named acct profile associated with the protocol_plugin provider will have its values passed to ``ctx.acct`` of the appropriate cache function.

File sources that have a mimetype, such as zip files, will be unarchived before further processing.

This is an example of an idem config file that specifies ``sls_sources`` and ``param_sources``:

.. code-block:: yaml

    idem:
      sls_sources:
        - file://path/to/sls_tree
        - file://path/to/sls_source.zip
        - git://github.com/my_user/my_project.git
        - git+http://github.com/my_user/my_project.git
        - git+https://github.com/my_user/my_project.git
      param_sources:
        - file://path/to/sls_tree
        - file://path/to/sls_source.zip
        - git://github.com/my_user/my_project.git
        - git+http://github.com/my_user/my_project.git
        - git+https://github.com/my_user/my_project.git

``sls_sources`` and ``param_sources`` can also be specified from the CLI.

.. note

    CLI options override config file definitions.

.. code-block:: bash

    $ idem state my.sls.ref \
          --sls-sources \
          "file://path/to/sls_tree" \
          "file://path/to/sls_source.zip" \
          "git://github.com/my_user/my_project.git" \
          "git+http://github.com/my_user/my_project.git"
          "git+https://github.com/my_user/my_project.git"
          --param-sources \
          "file://path/to/sls_tree" \
          "file://path/to/sls_source.zip" \
          "git://github.com/my_user/my_project.git"
          "git+http://github.com/my_user/my_project.git"
          "git+https://github.com/my_user/my_project.git"
