=====================
Idem Describe Command
=====================

The ``idem describe`` command lists the resources in a cloud account.

The ``idem describe`` command supports state file paths and regular expressions as input. In addition, you can refine output by applying a filter.

State File Path as Input
========================

The following command returns all AWS S3 buckets in the provided cloud account.

.. code-block:: bash

    $ idem describe aws.s3.bucket

Regular Expression as Input
===========================

Any valid regular expression can be used as an input. The following command returns all AWS resources in the provided cloud account.

.. code-block:: bash

    $ idem describe "aws.*"

The following command returns all dynamodb and S3 resources in the provided cloud account.

.. code-block:: bash

    $ idem describe "aws\.(dynamodb|s3)\..*"

Filtering
=========

To further refine output, you can also add the ``--filter`` argument.

The following command only returns automatically named S3 buckets that start with the word *production*.

.. code-block:: bash

    $ idem describe aws.s3.bucket --filter="[?resource[?bucket_prefix=='production']]"
