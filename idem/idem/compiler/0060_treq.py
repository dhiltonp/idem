def stage(hub, name: str):
    """
    Apply the transparent requisites for resources and their contracts
    """
    low = hub.idem.ccomps.treq.apply(
        hub.idem.RUNS[name]["subs"],
        hub.idem.RUNS[name]["low"],
    )
    hub.idem.RUNS[name]["low"] = low
